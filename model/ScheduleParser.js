var Match=require('./Match.js');
var Round=require('./Round.js');
var Constants=require('./Constants.js');
var fs = require('fs');
var cheerio = require('cheerio');
var format = require('date-fns/format');

const YELLOW_TEES = ["Birkholz", "Frost", "Pearlman"];

function getMatches(file) {
  text = fs.readFileSync(file, 'utf8');

  return parseMatches(text);
}

function parseMatches(text) {

  $ = cheerio.load(text);

 courses = [];

  $('h2').each(function(i, elem) {
    course = $(this).text();
    if (Constants.COURSES.includes(course)) {
      courses.push(course);
    } else {
      console.log("Unknown course at h2: " + course);
    }
  });

  return parseTable($, courses);

}

function parseTable($, courses) {

  var data = [];
  var matchCount = 0;

  var team1 = "";
  var team2 = "";
  var players1 = [];
  var players2 = [];
  var handicaps1 = [];
  var handicaps2 = [];

  var matches = [];

  $('tr').each(function(i, tr){
      var children = $(this).children();
      for (c=0; c < children.length; c++) {
        text = $(children[c]).text().trim();
        if (text.length > 0 && !text.startsWith("_")) {
          if (c == 1) {
            team1 = text;
          } else if (c == 5) {
            team2 = text;
          } else if (c == 2) {
            players1.push(text);
          } else if (c == 6) {
            players2.push(text);
          } else if (c == 4) {
            handicaps1.push(parseHandicap(text));
          } else if (c == 8) {
            handicaps2.push(parseHandicap(text));
          }
        }
      }

      if (players1.length == 2) {
        temp = [];
        temp.push(players1[0]);
        temp.push(players1[1]);
        temp.push(players2[0]);
        temp.push(players2[1]);
        tees = [];
        for (var i=0; i < temp.length; i++) {
          tees.push(YELLOW_TEES.indexOf(temp[i]) >= 0 ? Constants.YELLOW : Constants.BLUE);
        }
        handicaps = [];
        handicaps.push(handicaps1[0]);
        handicaps.push(handicaps1[1]);
        handicaps.push(handicaps2[0]);
        handicaps.push(handicaps2[1]);

        c = matchCount < 4 ? courses[0] : courses[1];

        m = new Match("today", temp, c, tees, handicaps);
        matches.push(m);

        players1 = [];
        players2 = [];
        handicaps1 = [];
        handicaps2 = [];
        matchCount++;
      }
  });

  return matches;
}

function parseHandicap(h) {
   h = h.substring(1, h.length - 2);
   return parseInt(h.trim());
}

module.exports = {
  getMatches : getMatches,
  parseMatches : parseMatches
};
