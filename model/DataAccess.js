const ScheduleParser=require("./ScheduleParser");
const Match=require("./Match");
const Round=require("./Round");

var matches;

function getCurrentMatches() {
  return matches;
};

function saveCurrentMatches(m) {
   matches = m;
};

function swapAB(matchId, team) {
  if (matches == null) {
    console.log("matches not loaded");
    return;
  }

  var m = matches[matchId];
  m.swapAB(team);
}

function updateScores(matchId, hole, scores) {
  if (matches == null) {
    console.log("matches not loaded");
    return;
  }

  var m = matches[matchId];
  for (i=0; i < scores.length; i++) {
    m.rounds[i].setScore(hole, scores[i]);
  }

  m.setCurrentHole(hole + 1);
}

module.exports = {
  getCurrentMatches : getCurrentMatches,
  updateScores : updateScores,
  swapAB : swapAB,
  saveCurrentMatches : saveCurrentMatches
};
