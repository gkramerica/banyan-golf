var Match=require('./Match.js');
var Round=require('./Round.js');
var Constants=require('./Constants.js');

function MatchHelper(m) {
  this.match = m;
}

MatchHelper.prototype.getScores = function getScores(player) {
  return this.match.rounds[player].scores;
}

MatchHelper.prototype.getHandicapStrokes = function getHandicapStrokes(player) {
   var strokes = [0,0,0,0,0,0,0,0,0];
   var r = this.match.rounds[player];
   var o = this.match.rounds[getOpponent(player)];

   if (r.handicap <= o.handicap) {
     return strokes;
   }

   var h = r.handicap - o.handicap;
   var tee = o.Tee;
   var c = this.getCourseIndex();

   return getStrokes(tee, h, c);
}

function getStrokes(tee, h, courseIndex) {
  var strokes = [0,0,0,0,0,0,0,0,0];
  var handicaps = getHandicaps(tee == Constants.BLUE ? Constants.HOLE_HANIDCAPS_BLUE[courseIndex] : Constants.HOLE_HANIDCAPS_RED[courseIndex]);

  var min = Math.floor(h / 9);
  var extra = h % 9;

  for (var i=0; i < handicaps.length; i++) {
    strokes[i] = min;
    if (handicaps[i] <= extra ) {
      strokes[i]++;
    }
  }

 return strokes;
}


MatchHelper.prototype.getNetScores = function getNetScores(player) {
  var r = this.match.rounds[player];

  var matchStrokes = this.getHandicapStrokes(player);

  var strokes = getStrokes(r.tee, r.handicap, this.getCourseIndex());
  var scores = this.getScores(player);

  var net = [];
  var total = 0;

  for (var i=0; i < scores.length; i++) {
    var s = scores[i];
    if (s > 0) {
      total = total + s - strokes[i];
      s = s - matchStrokes[i];
    }
    net.push(s);
  }

  net.push(total);

  return net;
}

MatchHelper.prototype.getCourseIndex = function getCourseIndex() {
   return Constants.COURSES.indexOf(this.match.course);
}

MatchHelper.prototype.getPars = function getPars() {
  return Constants.HOLE_PAR[this.getCourseIndex()];
}

MatchHelper.prototype.getHandicaps = function getHandicaps() {
  return Constants.HOLE_HANIDCAPS_BLUE[this.getCourseIndex()];
}

MatchHelper.prototype.getPoints = function getPoints(player) {
  var player2 = player > 1 ? player - 2 : player + 2;

  var net1 = this.getNetScores(player);
  var net2 = this.getNetScores(player2);

  var scores = [];

  for (var i=0; i < net1.length; i++) {
    if (net1[i] < net2[i]) {
      scores.push(2);
    } else if (net1[i] > net2[i]) {
      scores.push(0);
    } else {
      scores.push(1);
    }
  }

  return scores;
}

MatchHelper.prototype.getTeamPoints = function getTeamPoints(team) {
  var t1 = team == 0 ? 0 : 2;
  var t2 = team == 0 ? 2 : 0;

  var net1a = this.getNetScores(t1);
  var net1b = this.getNetScores(t1 + 1);
  var net2a = this.getNetScores(t2);
  var net2b = this.getNetScores(t2 + 1);

  var scores = [];

  for (var i=0; i < net1a.length; i++) {
    var n1 = net1a[i] + net1b[i];
    var n2 = net2a[i] + net2b[i];

    if (n1 < n2) {
      scores.push(2);
    } else if (n1 > n2) {
      scores.push(0);
    } else {
      scores.push(1);
    }
  }

  return scores;
}


MatchHelper.prototype.getTotalPoints = function getTotalPoints(team) {
  var a = this.getPoints(team == 0 ? 0 : 2);
  var b = this.getPoints(team == 0 ? 1 : 3);
  var t = this.getTeamPoints(team);

  var totals = [];

  for (i=0; i < a.length; i++) {
    totals[i] = a[i] + b[i] + t[i];
  }

  return totals;
}

MatchHelper.prototype.getFinalScore = function getFinalScore(team) {
  var t = this.getTotalPoints(team);

  var total = 0;

  for (i=0; i < t.length; i++) {
    total += t[i];
  }

  return total;
}

function getHandicaps(handicaps) {
  var list = [];
  for (var i=0; i < handicaps.length; i++) {
    var h = handicaps[i];
    if (h % 2 == 1) {
      h++;
    }
    list.push(h / 2);
  }
  return list;
}

function getOpponent(player) {
  if (player == 0) {
    return 2;
  }
  if (player == 1) {
    return 3;
  }
  if (player == 2) {
    return 0;
  }

  return 1;
}

module.exports = MatchHelper;
