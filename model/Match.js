var Round=require('./Round.js');

var numHoles = 9;

function Match(date, players, course, tees, handicaps) {
  this.date = date;
  this.course = course;
  this.currentHole = 0;
  this.rounds = [];

  for (var i=0, len=players.length; i < len; i++) {
    r = new Round(players[i], tees[i], handicaps[i], numHoles);
    this.rounds.push(r);
  }
}

Match.prototype.getTitle = function getTitle() {
  return this.rounds[0].player + "/" + this.rounds[1].player + " vs " + this.rounds[2].player + "/" + this.rounds[3].player;
}

Match.prototype.getCurrentHole = function getCurrentHole() {
  return this.currentHole;
}
Match.prototype.setCurrentHole = function setCurrentHole(hole) {
  this.currentHole = hole;
}

Match.prototype.swapAB = function swapAB(team) {
  var p = team == 0 ? 0 : 2;
  var temp = this.rounds[p];
  this.rounds[p] = this.rounds[p+1];
  this.rounds[p+1] = temp;
}


module.exports = Match;
