
const RIVER_FRONT="Riverside Front";
const RIVER_BACK="Riverside Back";
const LAKE_FRONT="Lakeside Front";
const LAKE_BACK="Lakeside Back";

const BLUE="Blue";
const YELLOW="Yellow";
const RED="Red";

const SCHEDULE_URL="http://leaguegolf.net/BGL/Latest_Schedule.html";

const COURSES = [LAKE_FRONT, LAKE_BACK, RIVER_FRONT, RIVER_BACK];
const TEES = [BLUE, YELLOW, RED];

const HOLE_HANIDCAPS_BLUE = [
  [10,6,12,2,14,8,4,16,18],
  [9,15,17,7,1,11,5,3,13],
  [4,6,2,8,12,16,18,10,14],
  [3,5,7,13,15,17,9,1,11]
];

const HOLE_HANIDCAPS_RED = [
  [12,6,8,4,16,10,2,18,14],
  [13,5,9,7,3,15,11,1,17],
  [4,14,6,2,18,10,16,8,12],
  [3,5,11,13,7,17,1,15,9]
];

const HOLE_PAR = [
  [3,5,4,4,3,4,5,3,4],
  [4,4,4,4,3,5,4,5,3],
  [4,4,5,4,3,4,3,4,4],
  [5,4,4,4,4,3,4,3,5]
];

module.exports = {
  HOLE_PAR : HOLE_PAR,
  HOLE_HANIDCAPS_BLUE : HOLE_HANIDCAPS_BLUE,
  HOLE_HANIDCAPS_RED : HOLE_HANIDCAPS_RED,
  SCHEDULE_URL : SCHEDULE_URL,
  RIVER_FRONT : RIVER_FRONT,
    RIVER_BACK : RIVER_BACK,
    LAKE_FRONT : LAKE_FRONT,
    BLUE : BLUE,
    YELLOW : YELLOW,
    RED : RED,
    COURSES : COURSES,
    TEES : TEES
};
