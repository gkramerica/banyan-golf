

function Round(player, tee, h, numHoles) {
  this.player = player;
  this.tee = tee;
  this.handicap = h;
  this.scores = [];

  for (i=0; i < numHoles; i++) {
    this.scores.push(0);
  }
}

Round.prototype.getScore = function getScore(hole) {
  return this.scores[hole];
}

Round.prototype.setScore = function setScore(hole, score) {
  this.scores[hole] = score;
}

module.exports = Round;
