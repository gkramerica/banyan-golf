var assert = require('assert');
var ScheduleParser = require('../model/ScheduleParser');
var MatchHelper = require('../model/MatchHelper');
var Constants = require('../model/Constants');

describe('MatchHelper', function() {
  describe('Handicaps', function() {
    var matches = ScheduleParser.getMatches('./Latest_Schedule.html');
    var m = matches[3];

    it('check title', function() {
      assert.equal(m.getTitle(), "Birkholz/Levinson vs Adams/Asher");
    });

    it('check course', function() {
      assert.equal(m.course, Constants.LAKE_FRONT, "Wrong course");
    });

    it('check handicaps', function() {
      var expected = [11,19,9,19];
      for (var i=0; i < expected.length; i++) {
        assert.equal(m.rounds[i].handicap, expected[i], "Wrong handicap for " + m.rounds[i].player);
      }
    });

    var mh = new MatchHelper(m);

    it('check handicaps', function() {
      var expected = [
        [0,0,0,1,0,0,1,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0]
      ];

      for (var i=1; i < expected.length; i++) {
        var strokes = mh.getHandicapStrokes(i);
        for (var z=0; z < expected[i].length; z++) {
          assert.equal(strokes[z], expected[i][z], "Wrong strokes for hole " + (z + 1) + " " + m.rounds[i].player);
        }
      }
    });

    it('check scores', function() {
      m.rounds[0].scores = [5,5,5,5,5,5,5,5,5];
      m.rounds[1].scores = [5,5,5,5,5,5,5,5,5];
      m.rounds[2].scores = [5,5,5,5,5,5,5,5,5];
      m.rounds[3].scores = [5,5,5,5,5,5,5,5,5];

      var expected = [
        [5,5,5,4,5,5,4,5,5, 34],
        [5,5,5,5,5,5,5,5,5, 26],
        [5,5,5,5,5,5,5,5,5, 36],
        [5,5,5,5,5,5,5,5,5, 26]
      ];

      for (var i=0; i < expected.length; i++) {
        var strokes = mh.getNetScores(i);
        for (var z=0; z < expected[i].length; z++) {
          assert.equal(strokes[z], expected[i][z], "Wrong net for hole " + (z + 1) + " " + m.rounds[i].player);
        }
      }
    });

  });

  describe('Handicaps2', function() {
    var matches = ScheduleParser.getMatches('./Latest_Schedule2.html');
    var m = matches[5];

    it('check title', function() {
      assert.equal(m.getTitle(), "Kramer/Bradley vs Valcour/Franke");
    });

    it('check course', function() {
      assert.equal(m.course, Constants.RIVER_BACK, "Wrong course");
    });

    it('check handicaps', function() {
      var expected = [8,9,9,11];
      for (var i=0; i < expected.length; i++) {
        assert.equal(m.rounds[i].handicap, expected[i], "Wrong handicap for " + m.rounds[i].player);
      }
    });

    var mh = new MatchHelper(m);

    it('check scores2', function() {
      m.rounds[0].scores = [7,5,5,5,5,4,5,4,6];
      m.rounds[1].scores = [5,6,6,5,4,4,4,2,5];
      m.rounds[2].scores = [5,6,5,5,5,3,5,6,6];
      m.rounds[3].scores = [6,6,7,5,5,5,5,4,5];

      var totals = [46,41,46,48];
      var nets = [46,41,45,46];
      var points = [8,15,12,5];

      for (var i=0; i < 4; i++) {
        var t = 0;
        var n = 0;
        var p = 0;

        var scores = mh.getScores(i);
        var netScores = mh.getNetScores(i);
        var matchPoints = mh.getPoints(i);

        for (var h=0; h < scores.length; h++) {
          t += scores[h];
          n += netScores[h];
          p += matchPoints[h];
        }

        p += matchPoints[9];

        assert.equal(t, totals[i], "Wrong score for " + i);
        assert.equal(n, nets[i], "Wrong net for " + i);

        assert.equal(p, points[i], "Wrong point total for " + i);

      }

      assert.equal(mh.getFinalScore(0), 33, "Wrong total score");
      assert.equal(mh.getFinalScore(1), 27, "Wrong total score");

      m.swapAB(0);
      assert.equal(m.getTitle(), "Bradley/Kramer vs Valcour/Franke");
      assert.equal(mh.getFinalScore(0), 35, "Wrong total score (switched)");
      assert.equal(mh.getFinalScore(1), 25, "Wrong total score (switched)");


    });

  });

});
