var assert = require('assert');
var ScheduleParser = require('../model/ScheduleParser');
var Constants = require('../model/Constants');

describe('ScheduleParser', function() {
  describe('getMatches()', function() {
    var matches = ScheduleParser.getMatches('./Latest_Schedule.html');

    it('check num matches', function() {

      assert.equal(matches.length, 8);
    });

    it('check title', function() {

      assert.equal(matches[0].getTitle(), "Kelly/Slawsky vs Kramer/Bradley");
    });

    it('check tees', function() {
      assert.equal(matches[0].rounds[0].tee, "Blue");
      assert.equal(matches[3].rounds[0].tee, "Yellow");
    });

    it('check handicaps', function() {

      assert.equal(matches[0].rounds[0].handicap, 6);
    });

    it('check course', function() {
      assert.equal(matches[0].course, Constants.LAKE_FRONT, "Wrong course " + matches[0].getTitle());
      assert.equal(matches[3].course, Constants.LAKE_FRONT, "Wrong course " + matches[3].getTitle());
      assert.equal(matches[6].course, Constants.RIVER_FRONT, "Wrong course " + matches[6].getTitle());
    });
  });
});

describe('Match', function() {
  describe('scoring start', function() {
    var matches = ScheduleParser.getMatches('./Latest_Schedule.html');
    var m = matches[0];
    scoreA = m.getPoints(0, "A");
    scoreB = m.getPoints(0, "B");
    scoreTeam = m.getPoints(0, "Team");

    it('A scoring', function() {
      assert.equal(scoreA.length, 10);
      assert.equal(scoreA[0], 0);
      assert.equal(scoreA[9], 0);
    });
    it('B scoring', function() {
      assert.equal(scoreB.length, 10);
      assert.equal(scoreB[0], 0);
    });
    it('Team scoring', function() {
      assert.equal(scoreTeam.length, 10);
      assert.equal(scoreTeam[0], 0);
    });
    it('Total scoring', function() {
      assert.equal(m.getTotals(1)[0], 0);
      assert.equal(m.getTotals(0)[0], 0);
      assert.equal(m.getTotals(1)[9], 0);
      assert.equal(m.getTotals(0)[9], 0);
    });
  });

  describe('scoring', function() {
    var matches2 = ScheduleParser.getMatches('./Latest_Schedule.html');
    var m = matches2[0];

    m.rounds[0].setScore(0,4);
    m.rounds[1].setScore(0,5);
    m.rounds[2].setScore(0,4);
    m.rounds[3].setScore(0,7);

    m.rounds[0].setScore(1,3);
    m.rounds[1].setScore(1,6);
    m.rounds[2].setScore(1,4);
    m.rounds[3].setScore(1,5);


    var scoreA = m.getPoints(0, "A");
    var scoreB = m.getPoints(0, "B");
    var scoreTeam1 = m.getPoints(0, "Team");
    var scoreTeam2 = m.getPoints(1, "Team");

    it('A scoring', function() {
      assert.equal(scoreA[0], 1, "Wrong A score on hole 1");
      assert.equal(scoreA[1], 2, "Wrong A score on hole 2");
      assert.equal(scoreA[9], 2, "Wrong A match score");
    });
    it('B scoring', function() {
      assert.equal(scoreB[0], 2);
      assert.equal(scoreB[1], 0);
      assert.equal(scoreB[9], 2);
    });
    it('Team scoring', function() {
      assert.equal(scoreTeam1[0], 2, "wrong team score on hole 1");
      assert.equal(scoreTeam1[9], 2, "wrong team total");
      assert.equal(scoreTeam2[0], 0, "wrong team score on hole 1");
      assert.equal(scoreTeam2[9], 0, "wrong team total");
    });
    it('Total scoring', function() {
      assert.equal(m.getTotals(0)[0], 5, "Hole 1, team 1");
      assert.equal(m.getTotals(1)[0], 1, "Hole 1, team 2");
      assert.equal(m.getTotals(0)[1], 3, "Hole 2, team 1");
      assert.equal(m.getTotals(1)[1], 3, "Hole 2, team 2");
      assert.equal(m.getTotals(0)[9], 6, "Total team 1");
      assert.equal(m.getTotals(1)[9], 0, "Total team 2");
    });
    it('Final score', function() {
      assert.equal(m.getFinalScore(0), 14, "team 1");
      assert.equal(m.getFinalScore(1), 4, "team 2");
    });

  });

  describe('blind scoring', function() {
    var matches2 = ScheduleParser.getMatches('./Latest_Schedule.html');
    var m = matches2[0];

    m.rounds[0].setScore(0,4);
    m.rounds[1].setScore(0,5);
    m.rounds[2].setScore(0,4);
    m.rounds[3].setScore(0,7);

    //m.rounds[0].setScore(1,3);
    m.rounds[1].setScore(1,6);
    m.rounds[2].setScore(1,4);
    m.rounds[3].setScore(1,5);

    var scoreA = m.getPoints(0, "A");
    var scoreB = m.getPoints(0, "B");
    var scoreTeam1 = m.getPoints(0, "Team");
    var scoreTeam2 = m.getPoints(1, "Team");

    it('A scoring', function() {
      assert.equal(scoreA[0], 1, "Wrong A score on hole 1");
      assert.equal(scoreA[1], 0, "Wrong A score on hole 2");
      assert.equal(scoreA[9], 1, "Wrong A match score");
    });
    it('B scoring', function() {
      assert.equal(scoreB[0], 2);
      assert.equal(scoreB[1], 0);
      assert.equal(scoreB[9], 2);
    });
    it('Team scoring', function() {
      assert.equal(scoreTeam1[0], 2, "wrong team score on hole 1");
      assert.equal(scoreTeam1[9], 2, "wrong team total");
      assert.equal(scoreTeam2[0], 0, "wrong team score on hole 1");
      assert.equal(scoreTeam2[9], 0, "wrong team total");
    });
    it('Total scoring', function() {
      assert.equal(m.getTotals(0)[0], 5, "Hole 1, team 1");
      assert.equal(m.getTotals(1)[0], 1, "Hole 1, team 2");
      assert.equal(m.getTotals(0)[1], 0, "Hole 2, team 1");
      assert.equal(m.getTotals(1)[1], 2, "Hole 2, team 2");
      assert.equal(m.getTotals(0)[9], 5, "Total team 1");
      assert.equal(m.getTotals(1)[9], 1, "Total team 2");
    });
    it('Final score', function() {
      assert.equal(m.getFinalScore(0), 10, "team 1");
      assert.equal(m.getFinalScore(1), 4, "team 2");
    });

  });

});
