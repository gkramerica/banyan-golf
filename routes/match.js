var express = require('express');
var Match = require('../model/Match');
var Constants = require('../model/Constants');
var ScheduleParser = require('../model/ScheduleParser');
var router = express.Router();
var request = require("request");
var DataAccess = require("../model/DataAccess");
var MatchHelper = require("../model/MatchHelper");

router.get('/', function(req, res, next) {
  matchIndex = req.query.match;
  hole = req.query.hole;

  if (matchIndex == null) {
    matchIndex = 0;
  }

  var matchList = DataAccess.getCurrentMatches();

  if (matchList != null) {
    render(res, matchList, matchIndex);

  } else {
    request(Constants.SCHEDULE_URL, function(error, response, body) {
      matchList = ScheduleParser.parseMatches(body);
      DataAccess.saveCurrentMatches(matchList);

      render(res, matchList, matchIndex);
    });
  }
});

function render(res, matchList, matchIndex) {
  if (hole != null) {
    var m = matchList[matchIndex];
    m.setCurrentHole(hole);
  }

  var mh = new MatchHelper(matchList[matchIndex]);

  res.render('match', { matches: matchList, matchIndex: matchIndex, matchHelper: mh });

};

module.exports = router;
