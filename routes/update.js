var express = require('express');
var Match = require('../model/Match');
var Constants = require('../model/Constants');
var router = express.Router();
var request = require("request");
var DataAccess = require("../model/DataAccess");

router.post('/', function(req, res, next) {
  var matchList = DataAccess.getCurrentMatches();

  if (matchList != null) {
    matchIndex = +req.body.matchIndex;

    s = req.body.swap;


    if (s != null) {
      console.log("swapping AB " + s);

      DataAccess.swapAB(matchIndex, +s);
      res.status(200);
      res.send('success');
    } else {
      hole = +req.body.hole;

      console.log("updating match " + matchIndex + "hole=" + hole);

      scores = [];
      scores[0] = +req.body.score0;
      scores[1] = +req.body.score1;
      scores[2] = +req.body.score2;
      scores[3] = +req.body.score3;

      console.log("updating scores " +scores[0]);

      DataAccess.updateScores(matchIndex, hole, scores);
      res.status(200);
      res.send('success');
    }
  } else {
    console.log('no works');
    res.status(200);
    res.send('failed');
  }
});


module.exports = router;
